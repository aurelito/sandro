# Sandro project #

Web framework which can run the same code in both the client and the server, enabling several things like:

* Shared code for validations (running the ones that make sense both client-side and server side, while not writing them twice).

* HTML generation client-side and server side sharing the same code.

* Use browser libraries (like d3js) in the server.

## Technical details ##
For now is an eclipse "Dynamic Web Project" running on Tomcat 7. It needs the domino and the sandro-lib project in the same workspace to be developed.