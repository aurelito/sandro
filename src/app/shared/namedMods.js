define({d3:"external/d3", nm1:"./namedMods1", r:"require"}, function(m) {
  var b = m.d3.select("body")
  b.append("div").text("Generated on app/shared/namedMods")
  b.call(m.nm1)
  
  m.r({nm2:"./namedMods2"}, function(m) {
    b.call(m.nm2)
  })
  
  require({nm3:"app/shared/namedMods3"}, function (m) {
    b.call(m.nm3)
  })
})