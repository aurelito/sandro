define([], function() {
  return function(p) { p.clhyPage( function() {
    var d3 = p.d3
    d3.select("title").text("D3 server-side integration")
    d3.select("body").append("h1").text("D3 server-side integration")
    d3.select("body").append("p").text("A small demo showing how to use d3 running on the server")
  })}
})