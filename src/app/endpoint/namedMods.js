define([], function() {
  return function(p) { p.clhyPage( function() {

    var d3 = p.d3
    var startClientJs = p.startClientJs
    
    d3.select("body").append("div").text("Named mods - Generated on the server")
    startClientJs("app/shared/namedMods")
  })}
})