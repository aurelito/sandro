define(["module"], function(module) {
  return function(p) { p.clhyPage( function() {
    var document = p.document

    var title = document.createElement("title")
    title.appendChild(document.createTextNode("Domino demo"))
    document.head.appendChild(title)
    
    var h1 = document.createElement("h1")
    h1.appendChild(document.createTextNode("Domino demo"))
    document.body.appendChild(h1)
    
    var pp = document.createElement("p")
    pp.appendChild(document.createTextNode(
      "This is a demo of Sandro's domino integration. See " + module.id + " for details"
    ))
    document.body.appendChild(pp)

  })}
})