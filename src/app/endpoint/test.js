define(
  ["sandro/asi", "app/tests", "sandro/nadaMas/array"], 
  function(asi, tests, array) {
    return function(p) { p.clhyPage( function() {
      var d3 = p.d3
      
      var result = asi.run(tests)
      
      d3.select("title").text("Sandro tests")
      d3.select("body").append("h1").text("Server-side tests")
      var serverResultselectionParent = d3.select("body").append("div").attr("id","server-results")
      var results = serverResultselectionParent.selectAll(function() { return this.childNodes }).data(result)
      
      results.enter().append("div")
      results.exit().remove()
      
      var names = results.selectAll(".name").data(function(d){ return [d.name]})
      names.enter().append("span").classed("name",true)
      names.exit().remove()
      names.text(function(d) {return d})
      
      var statuses = results.selectAll(".status").data(function(d) {return [d.status]})
      statuses.enter().append("span").classed("status",true)
      statuses.exit().remove()
      statuses.text(function(d) {return " " + d})
      
      var causes = results.selectAll(".cause").data(function(d) {return d.cause ? [d.cause] : []})
      causes.enter().append("span").classed("cause",true)
      causes.exit().remove()
      causes.text(function(d) {return " " + d})
      
    })}
  }
)