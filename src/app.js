define(
  ["sandro/comoLoHiceYo", "sandro/alFinal", "app/config",  "sandro/nadaMas/string", ], 
  function(clhy, alFinal, config){
    var router = null
    
    var init = function(servlet, config) {
      
      router = clhy()

      if (config.devel) {
        router = function(p) {
          var req = p.request
          var resp = p.response
          
          // Set standard HTTP/1.1 no-cache headers.
          resp.setHeader("Cache-Control", "no-cache")
          // Set standard HTTP/1.0 no-cache header.
          resp.setHeader("Pragma", "no-cache")
          router(p)
        } 
      }
      
    }
    
    return {
      init: init,
      service: function(p) { router(p) } // Needs late binding because init is setting router
    }
  }
)